# Webcam motion detector
* [Subject](#subject)
* [Covered topics](#covered-topics)
* [Libraries](#libraries)

# Subject   :pushpin:
Starts a webcam and detects movements. Store the time events and present them in a graph


# Covered topics
- Webcam control
- Video display and capture
- Frame analysis
- Manipulation of csv files
- Data visualization with interactive graphs

# Libraries :books:
- openCV
- Pandas
- Bokeh