from bokeh.plotting import figure
from bokeh.io import output_file, show
from bokeh.models import HoverTool, ColumnDataSource

def create_graph(data_frame):
    # Fetch x and y coordinates from data frame
    x = data_frame.Start
    y = data_frame.End

    # Convert dates to string
    data_frame["Start_string"] = data_frame["Start"].dt.strftime("%Y-%m-%d %H:%M:%S")
    data_frame["End_string"] = data_frame["End"].dt.strftime("%Y-%m-%d %H:%M:%S")

    # Convert data to columns for better hovering functionalities
    column_data_source = ColumnDataSource(data_frame)

    # Hovering
    hover = HoverTool(tooltips=[("Begin", "@Start_string"), ("End", "@End_string")])

    # Graphic setup
    graph = figure(x_axis_type="datetime",
                   height=600,
                   width=1000,
                   title="Motion graph")

    graph.quad(left="Start", # data_frame.Start
               right="End",  # data_frame.End
               top=1,
               bottom=0,
               color="green",
               source=column_data_source)

    # Add hover tool
    graph.add_tools(hover)

    # Create an html file
    output_file("Graph.html")

    # Display graph
    show(graph)