import cv2
import datetime
import pandas

def capture_webcam():
    # Capture webcam video : arg is the index of the camera
    video = cv2.VideoCapture(0)

    # Keep 1st frame as a reference to detect movement
    ref_frame = None
    # Records movements times
    times = []
    # Store times in pandas data frame
    data_frame = pandas.DataFrame(columns=["Start", "End"])
    # Keep tracks of detected mouvements
    detections = [None, None] # To avoid out of range in [-2]




    # Loop on each frame
    while True:
        # Get image from video
        check, frame = video.read()

        # Track status for detection times
        detected = False

        # Use gray scale for better detection
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        # Remove noise on the img
        gray = cv2.GaussianBlur(gray, (21, 21), 0)

        # Save 1st frame
        if ref_frame is None:
            ref_frame = gray
            continue

        # Compare with the reference frame
        delta_frame = cv2.absdiff(ref_frame, gray)
        # Define a threshold for movement : assign white pixel (255)
        thres_frame = cv2.threshold(delta_frame, 100, 255, cv2.THRESH_BINARY)[1]
        # Smooth image by dilating non relevant movement
        thres_frame = cv2.dilate(thres_frame, None, iterations=2)
        # Select areas contours, store in a tuple
        # Better use a copy of the image instead of the original one
        (cnts, _) = cv2.findContours(thres_frame.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        for contour in cnts:
            if cv2.contourArea(contour) < 1000:
                continue
            detected = True
            # Draw a rectangle around the moving area
            (x, y, w, h) = cv2.boundingRect(contour)
            # Draw the rectangle
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 3)

        # Show frame
        cv2.imshow("Capture", frame)
        cv2.imshow("Delta", delta_frame)
        cv2.imshow("Threshold", thres_frame)

        detections.append(detected)
        detections = detections[-2:] #Only save the last 2 items

        # Beginning of movement
        if detections[-1] and not detections[-2]:
            times.append(datetime.datetime.now())
        # End of movement
        if not detections[-1] and detections[-2]:
            times.append(datetime.datetime.now())

        # Press q to quit
        key = cv2.waitKey(1)
        if key == ord('q'):
            if detected:
                times.append(datetime.datetime.now())
            break

    # Save times
    for i in range(0, len(times), 2):
        data_frame = data_frame.append({'Start' : times[i], 'End' : times[i+1]}, ignore_index=True)


    data_frame.to_csv("times.csv")
    # Close window
    cv2.destroyAllWindows()
    # Release webcam at the end of the  program
    video.release()
    return(data_frame)